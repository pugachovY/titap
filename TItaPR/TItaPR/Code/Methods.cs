﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TItaPR.DBContext;
using TItaPR.Models.Entity;
using TItaPR.Models.ViewModels;

namespace TItaPR.Code
{
    public static class Methods
    {
        /// <summary>
        ///1: ANum 2:number of lost round сначала выбыл первый, потом 2, 3 ... 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ElectionsViewModel> Elections(AppDbContext context)
        {
            var electionResult = new List<ElectionsViewModel>();//return value

            int count = context.Alternatives.Count();
            var electionAlterntive = new Dictionary<int, int>();//Anum, voteCount
            foreach (var alternative in context.Alternatives)
            {
                electionAlterntive.Add(alternative.ANum, 0);
            }

            for (int i = 0; i < count; i++)
            {
                foreach (var user in context.LPRs)
                {
                    var userResult = context.Results.Where(r => r.LNum == user.LNum).OrderBy(r => r.Range).ToList();//результаты польз. отсортированые по предпочтениям
                    foreach (var vote in userResult)//голосуем за те альтернативы которые остались
                    {
                        if (electionAlterntive.ContainsKey(vote.ANum))
                        {
                            electionAlterntive[vote.ANum] += 1;//отдаем госол
                            break;
                        }
                    }
                }
                //после голосования выбывает САМОЕ СЛАБОЕ ЗВЕНО
                var lastAltKey = electionAlterntive.OrderBy(x => x.Value).FirstOrDefault().Key;

                electionResult.Add(new ElectionsViewModel {
                    Alternative = context.Alternatives.Find(lastAltKey),
                    Position = count- i
                });

                electionAlterntive.Remove(lastAltKey);
                foreach (var key in electionAlterntive.Keys.ToList())
                {
                    electionAlterntive[key] = 0;
                }

            }

            return electionResult;
        }
    }
}