﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TItaPR.Models.Entity;

namespace TItaPR.Models.ViewModels
{
    public class ElectionsViewModel
    {
        public Alternative Alternative { get; set; }
        public int Position { get; set; }
    }
}