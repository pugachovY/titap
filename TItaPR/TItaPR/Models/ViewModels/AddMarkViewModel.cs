﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TItaPR.Models.ViewModels
{
    public class AddMarkViewModel
    {
        public int CNum { get; set; }
        public int ANum { get; set; }
        public int NumMark { get; set; }
        public string MName { get; set; }
    }
}