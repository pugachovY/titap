﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TItaPR.Models.Entity;

namespace TItaPR.Models.ViewModels
{
    public class MarkAddingFormViewModel
    {
        public List<Alternative> Alternatives { get; set; }
        public List<Criterion> Criterions { get; set; }
    }
}