﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TItaPR.Models.Entity
{
    public class Vector
    {
        [Key]
        public int VNum { get; set; }
        public int ANum{ get; set; }
        [ForeignKey("ANum")]
        public Alternative Alternative { get; set; }

        public int MNum { get; set; }
        [ForeignKey("MNum")]
        public Mark Mark{ get; set; }

    }
}