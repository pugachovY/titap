﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TItaPR.Models.Entity
{
    public class Result
    {
        [Key]
        public int RNum { get; set; }
        
        public int LNum{ get; set; }
        [ForeignKey("LNum")]
        public LPR LPR { get; set; }

        public int ANum { get; set; }
        [ForeignKey("ANum")]
        public Alternative Alternative { get; set; }
        public int Range { get; set; }
        public double AWeight { get; set; }
    }
}