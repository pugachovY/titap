﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TItaPR.Models.Entity
{
    public class LPR
    {
        [Key]
        public int LNum { get; set; }
        public string Lname { get; set; }
        public int LRange { get; set; }
        public ICollection<Result>Results { get; set; }
    }
}