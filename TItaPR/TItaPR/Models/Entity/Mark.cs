﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TItaPR.Models.Entity
{
    public class Mark
    {
        [Key]
        public int MNum { get; set; }
        public int CNum { get; set; }
        public string MName { get; set; }
        public int MRange { get; set; }
        public int NumMark { get; set; }
        public double NormMark { get; set; }
        public ICollection<Vector> Vectors{ get; set; }

        [ForeignKey("CNum")]
        public Criterion Criterion { get; set; }

    }
}