﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TItaPR.Models.Entity
{
    public class Alternative
    {
        [Key]
        public int ANum { get; set; }
        public string  AName { get; set; }
        public ICollection<Vector> Vectors { get; set; }
        public ICollection<Result> Results { get; set; }
    }
}