﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TItaPR.Models.Entity;

namespace TItaPR.DBContext
{
    public class AppDbContext: DbContext
    {
        public AppDbContext()
            :base("DefaultConnection")
        {
        }

        public DbSet<Alternative> Alternatives { get; set; }
        public DbSet<Criterion> Criterions { get; set; }
        public DbSet<LPR> LPRs { get; set; }
        public DbSet<Vector> Vectors { get; set; }
        public DbSet<Result> Results{ get; set; }
        public DbSet<Mark> Marks { get; set; }

    }
}