﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TItaPR.DBContext;
using TItaPR.Models.Entity;

namespace TItaPR.Controllers
{
    public class LPRsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: LPRs
        public ActionResult Index()
        {
            return View(db.LPRs.ToList());
        }

        // GET: LPRs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LPR lPR = db.LPRs.Find(id);
            if (lPR == null)
            {
                return HttpNotFound();
            }
            return View(lPR);
        }

        // GET: LPRs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LPRs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LNum,Lname,LRange")] LPR lPR)
        {
            if (ModelState.IsValid)
            {
                db.LPRs.Add(lPR);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lPR);
        }

        // GET: LPRs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LPR lPR = db.LPRs.Find(id);
            if (lPR == null)
            {
                return HttpNotFound();
            }
            return View(lPR);
        }

        // POST: LPRs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LNum,Lname,LRange")] LPR lPR)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lPR).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lPR);
        }

        // GET: LPRs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LPR lPR = db.LPRs.Find(id);
            if (lPR == null)
            {
                return HttpNotFound();
            }
            return View(lPR);
        }

        // POST: LPRs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LPR lPR = db.LPRs.Find(id);
            db.LPRs.Remove(lPR);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SelectUser(int userId)
        {
            Session["CurrentUserId"] = userId;
            var user = db.LPRs.Find(userId);
            Session["CurrentUserName"] = user.Lname;
            return RedirectToAction("Index", "Alternatives");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
