﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TItaPR.DBContext;
using TItaPR.Models.Entity;
using TItaPR.Models.ViewModels;

namespace TItaPR.Controllers
{
    public class MarksController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Marks
        public ActionResult Index()
        {
            var marks = db.Marks.Include(m => m.Criterion)
                .Include(m => m.Vectors)
                .Include(m => m.Vectors.Select(v => v.Alternative));
            return View(marks);
        }

        // GET: Marks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            return View(mark);
        }

        // GET: Marks/Create
        public ActionResult Create()
        {
            var model = new MarkAddingFormViewModel()
            {
                Alternatives = db.Alternatives.ToList(),
                Criterions = db.Criterions.ToList()
            };
            return View(model);
        }

        // POST: Marks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddMarkViewModel model)
        {
            if (ModelState.IsValid)
            {
                var mark = new Mark
                {
                    NumMark = model.NumMark,
                    CNum = model.CNum,
                    MRange = 1,
                    MName = model.MName,
                    NormMark = 0
                };
                db.Marks.Add(mark);
                db.SaveChanges();
                db.Vectors.Add(new Vector() {ANum = model.ANum, MNum = mark.MNum});
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Marks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            return View(mark);
        }

        // POST: Marks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MNum,CNum,MName,MRange,NumMark,NormMark")] Mark mark)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mark).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mark);
        }

        // GET: Marks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            return View(mark);
        }

        // POST: Marks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mark mark = db.Marks.Find(id);
            db.Marks.Remove(mark);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
