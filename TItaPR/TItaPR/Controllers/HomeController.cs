﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TItaPR.Code;
using TItaPR.DBContext;
using TItaPR.Models.Entity;

namespace TItaPR.Controllers
{
    public class HomeController : Controller
    {
        AppDbContext context = new AppDbContext();
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult ShowResults()
        {
            
            var results = context.Results
                .Include(c => c.Alternative)
                .Include(c=>c.LPR).ToList();
            return View(results);
        }
        public ActionResult MaxMinMethod()
        {
            MaxMin();
            return RedirectToAction("ShowResults");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Правило відносної більшості з вибуванням ";
            var model = Methods.Elections(context).OrderBy(x => x.Position).ToList();
            //model = model.OrderBy(x => x.Position).ToList();
            return View(model);
        }
        public KeyValuePair<Alternative, double> MaxMin()
        {
            var minElements = new Dictionary<Alternative, double>();
            foreach (var criterion in context.Criterions)
            {
                var marks = context.Marks.Where(m => m.CNum == criterion.CNum);
                var normCoef = marks.Sum(m => m.NumMark);
                foreach (var mark in marks)
                {
                    mark.NormMark = (double)mark.NumMark / normCoef;
                    context.Entry(mark).State= EntityState.Modified;
                }
            }
            context.SaveChanges();
            foreach (var alt in context.Alternatives)
            {
                var marks = new List<Mark>();
                var marksIdList = context.Vectors
                    .Where(v => v.ANum == alt.ANum)
                    .Select(v => v.MNum);
                foreach (var markId in marksIdList)
                {
                    marks.Add(context.Marks.Find(markId));
                }
                minElements.Add(alt, marks.Select(m => m.NormMark).Min());
            }
            var result = minElements.First(v => v.Value == minElements.Values.Max());
            var userId = (int)Session["CurrentUserId"];
            context.Results.Add(new Result
            {
                LNum = userId,
                AWeight = result.Value,
                ANum = result.Key.ANum,
                Range = 99999
            });
            context.SaveChanges();
            return result;
        }
    }
}