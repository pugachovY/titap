﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TItaPR.DBContext;
using TItaPR.Models.Entity;

namespace TItaPR.Controllers
{
    public class VectorsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Vectors
        public ActionResult Index()
        {
            var vectors = db.Vectors.Include(v => v.Alternative).Include(v => v.Mark);
            return View(vectors.ToList());
        }

        // GET: Vectors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vector vector = db.Vectors.Find(id);
            if (vector == null)
            {
                return HttpNotFound();
            }
            return View(vector);
        }

        // GET: Vectors/Create
        public ActionResult Create()
        {
            ViewBag.ANum = new SelectList(db.Alternatives, "ANum", "AName");
            ViewBag.MNum = new SelectList(db.Marks, "MNum", "MName");
            return View();
        }

        // POST: Vectors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VNum,ANum,MNum")] Vector vector)
        {
            if (ModelState.IsValid)
            {
                db.Vectors.Add(vector);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ANum = new SelectList(db.Alternatives, "ANum", "AName", vector.ANum);
            ViewBag.MNum = new SelectList(db.Marks, "MNum", "MName", vector.MNum);
            return View(vector);
        }

        // GET: Vectors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vector vector = db.Vectors.Find(id);
            if (vector == null)
            {
                return HttpNotFound();
            }
            ViewBag.ANum = new SelectList(db.Alternatives, "ANum", "AName", vector.ANum);
            ViewBag.MNum = new SelectList(db.Marks, "MNum", "MName", vector.MNum);
            return View(vector);
        }

        // POST: Vectors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VNum,ANum,MNum")] Vector vector)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vector).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ANum = new SelectList(db.Alternatives, "ANum", "AName", vector.ANum);
            ViewBag.MNum = new SelectList(db.Marks, "MNum", "MName", vector.MNum);
            return View(vector);
        }

        // GET: Vectors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vector vector = db.Vectors.Find(id);
            if (vector == null)
            {
                return HttpNotFound();
            }
            return View(vector);
        }

        // POST: Vectors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vector vector = db.Vectors.Find(id);
            db.Vectors.Remove(vector);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
